## Code of conduct

Persian Telos is consist of IT experts in various fields to build projects for Telos blockchain.
Most of our projects are exclusively for Telos blockchain.

Also, we want to create Farsi community for Telos for who speak Farsi in the world and we will support anyone who wants to work for Telos from Farsi regions.
